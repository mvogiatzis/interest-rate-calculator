# Interest Rate Calculator

You have been tasked with developing an interest rate calculator.

The calculator will calculate the outstanding balance of the loan after X months based on the original balance of the loan.

The interest rates are as follows:

 - if the balance is between £0 and £1000, the interest rate is 1%
 - if the balance is between £1000 and £2000, the interest rate is 2%
 - if the balance is greater than £2000, the interest rate is 5%

For example, if the balance of the account was £550.00, the interest payable should be £5.50 (i.e. 1% of £550.00) after one month. Therefore the outstanding balance should be £555.50.