package com.zedtechnologies;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class LoanBalanceCalculatorTest {

    @InjectMocks
    private LoanBalanceCalculator loanBalanceCalculator;

    @Mock
    private InterestRateCalculator interestRateCalculator;

    @Test(expected = RuntimeException.class)
    public void calculateLoanBalance_should_throwException_when_negativeBalance(){
        loanBalanceCalculator.calculateLoanBalance(new BigDecimal("-15.00"), 1);
    }

    @Test(expected = RuntimeException.class)
    public void calculateLoanBalance_should_throwException_when_balanceNull(){
        loanBalanceCalculator.calculateLoanBalance(null, 1);
    }

    @Test(expected = RuntimeException.class)
    public void calculateLoanBalance_should_throwException_when_monthsZero(){
        loanBalanceCalculator.calculateLoanBalance(new BigDecimal("15.00"), 0);
    }

    @Test(expected = RuntimeException.class)
    public void calculateLoanBalance_should_throwException_when_monthsNegative(){
        loanBalanceCalculator.calculateLoanBalance(new BigDecimal("15.00"), -1);
    }

    @Test
    public void calculateLoanBalance_should_returnCorrectAmount_when_initialBalance_1_month(){
        given(interestRateCalculator.calculateInterest(new BigDecimal("1000.00"))).willReturn(new BigDecimal("10.00"));

        //when
        final BigDecimal actual = loanBalanceCalculator.calculateLoanBalance(new BigDecimal("1000.00"), 1);

        //then
        assertEquals(new BigDecimal("1010.00"), actual);
    }

    @Test
    public void calculateLoanBalance_should_returnCorrectAmount_when_initialBalance_2_month(){
        given(interestRateCalculator.calculateInterest(new BigDecimal("1000.00"))).willReturn(new BigDecimal("10.00"));
        given(interestRateCalculator.calculateInterest(new BigDecimal("1010.00"))).willReturn(new BigDecimal("10.10"));

        //when
        final BigDecimal actual = loanBalanceCalculator.calculateLoanBalance(new BigDecimal("1000.00"), 2);

        //then
        assertEquals(new BigDecimal("1020.10"), actual);
    }

}