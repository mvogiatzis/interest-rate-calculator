package com.zedtechnologies;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class InterestRateCalculatorTest {

    private InterestRateCalculator interestRateCalculator = new InterestRateCalculator();

    @Test
    public void calculateInterest_should_returnCorrectResult_when_BalanceBelow1000() {
        final BigDecimal actual = interestRateCalculator.calculateInterest(new BigDecimal("900.10"));
        assertTrue(new BigDecimal("9.0010").compareTo(actual) == 0);
    }

    @Test
    public void calculateInterest_should_returnCorrectResult_when_BalanceExactly1000() {
        final BigDecimal actual = interestRateCalculator.calculateInterest(new BigDecimal("1000"));
        assertTrue(new BigDecimal("10.00").compareTo(actual) == 0);
    }

    @Test
    public void calculateInterest_should_returnCorrectResult_when_BalanceBelow2000() {
        final BigDecimal actual = interestRateCalculator.calculateInterest(new BigDecimal("1510.00"));
        assertTrue(new BigDecimal("30.20").compareTo(actual) == 0);
    }

    @Test
    public void calculateInterest_should_returnCorrectResult_when_BalanceExactly2000() {
        final BigDecimal actual = interestRateCalculator.calculateInterest(new BigDecimal("2000.00"));
        assertTrue(new BigDecimal("40.00").compareTo(actual) == 0);
    }

    @Test
    public void calculateInterest_should_returnCorrectResult_when_BalanceAbove2000() {
        final BigDecimal actual = interestRateCalculator.calculateInterest(new BigDecimal("5000"));
        assertTrue(new BigDecimal("250").compareTo(actual) == 0);
    }
}