package com.zedtechnologies;

import java.math.BigDecimal;

public class Application {

    public static void main(String[] args) {
        LoanBalanceCalculator loanBalanceCalculator = new LoanBalanceCalculator(new InterestRateCalculator());
        final BigDecimal loanBalance = loanBalanceCalculator.calculateLoanBalance(new BigDecimal("1000.00"), 2);
        System.out.println("Load balance is " + loanBalance);
    }
}
