package com.zedtechnologies;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.math.BigDecimal.ZERO;
import static java.util.Optional.ofNullable;
import static java.util.stream.Stream.generate;

public class LoanBalanceCalculator {

  private InterestRateCalculator interestRateCalculator;

  public LoanBalanceCalculator(final InterestRateCalculator interestRateCalculator) {
    this.interestRateCalculator = interestRateCalculator;
  }

  public BigDecimal calculateLoanBalance(BigDecimal initialBalance, int months) {
    if (!validBalance(initialBalance) || !validMonths(months)) {
      throw new RuntimeException("Wrong input");
    }

    return calculate(initialBalance, months);
  }

  private boolean validBalance(final BigDecimal initialBalance) {
    return ofNullable(initialBalance)
        .filter(this::positiveBalance)
        .isPresent();
  }

  private BigDecimal calculate(BigDecimal balance, Integer months) {
    if (months <= 0) {
      return balance;
    }

    return calculate(balance.add(interestRateCalculator.calculateInterest(balance)), months - 1);
  }

  private boolean validMonths(final int months) {
    return months > 0;
  }

  private boolean positiveBalance(final BigDecimal initialBalance) {
    return initialBalance.compareTo(ZERO) >= 0;
  }


}
