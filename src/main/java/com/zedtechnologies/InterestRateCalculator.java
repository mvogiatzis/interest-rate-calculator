package com.zedtechnologies;

import java.math.BigDecimal;

public class InterestRateCalculator {

    private static final BigDecimal LOW_TIER_INTEREST_RATE = new BigDecimal("0.01");
    private static final BigDecimal MIDDLE_TIER_INTEREST_RATE = new BigDecimal("0.02");
    private static final BigDecimal HIGH_TIER_INTEREST_RATE = new BigDecimal("0.05");

    private static final BigDecimal LOW_TIER_BALANCE = new BigDecimal("1000.00");
    private static final BigDecimal MIDDLE_TIER_BALANCE = new BigDecimal("2000.00");

    public BigDecimal calculateInterest(final BigDecimal balance) {
        if (balance.compareTo(LOW_TIER_BALANCE) <= 0) {
            return LOW_TIER_INTEREST_RATE.multiply(balance);
        } else if (balance.compareTo(MIDDLE_TIER_BALANCE) <= 0) {
            return MIDDLE_TIER_INTEREST_RATE.multiply(balance);
        }
        return HIGH_TIER_INTEREST_RATE.multiply(balance);
    }

}
